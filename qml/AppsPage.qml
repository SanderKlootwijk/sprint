import QtQuick 2.4
import QtQuick.Layouts 1.1
import Qt.labs.settings 1.0
import AccountsService 0.1
import GSettings 1.0
import Ubuntu.Components 1.3

import Sprint 1.0
import "Components"

Page {
    id: appsPage

    property var appWidth: units.gu(10)
    property var appHeight: appWidth + units.gu(2)
    property var appToPlace: null
    property bool placingApp: false
    property int columns: appsPage.width / (appWidth + grid.columnSpacing)
    property int rows: (appsPage.height - bottomBar.height) / (appHeight + grid.rowSpacing)

    GSettings {
        id: systemSettings
        schema.id: 'com.ubuntu.touch.system-settings'
    }

    Settings {
        id: settings

        property string iconPackId

        onIconPackIdChanged: {
            IconPacks.setCurrentIconPack(settings.iconPackId);
        }
    }

    Component.onCompleted: {
        if (settings.iconPackId) {
            IconPacks.setCurrentIconPack(settings.iconPackId);
        }

        console.log('grid columns', columns, 'grid rows', rows)
        Applications.initialize(columns * rows, columns);
    }

    function showAppPage(app, index, launcher) {
        if (!placingApp) {
            var page = pageStack.push(Qt.resolvedUrl('AppPage.qml'), {app: app});
            page.onAddRemoveApp.connect(function() {
                if (launcher) {
                    Applications.insertLauncherApp(index, '')
                }
                else {
                    Applications.insertFavoriteApp(index, '')
                }
            });
        }
    }

    header: PageHeader {
        id: header
        visible: false
    }

    Rectangle {
        anchors.fill: parent
        color: systemSettings.dashBackground ? 'black' : 'white'

        Image {
            anchors.fill: parent

            visible: systemSettings.dashBackground
            source: AccountsService.backgroundFile
            fillMode: Image.PreserveAspectCrop
            opacity: systemSettings.backgroundOpacity
        }
    }

    GridLayout {
        id: grid

        anchors {
            top: parent.top
            right: parent.right
            left: parent.left
            margins: units.gu(1)
        }

        height: rows * (appHeight + rowSpacing)

        columns: appsPage.columns
        columnSpacing: units.gu(1)
        rowSpacing: units.gu(1)

        Repeater {
            model: Applications.launcherList

            delegate: Launcher {
                Layout.fillWidth: true
                Layout.preferredHeight: appHeight
                Layout.maximumWidth: appWidth

                app: model
                color: systemSettings.dashBackground ? 'white' : 'black'

                onPressAndHold: showAppPage(model, index, true)
            }
        }
    }

    Rectangle {
        anchors {
            left: parent.left
            right: parent.right
            bottom: bottomBar.top
        }

        height: 4

        color: theme.palette.normal.foreground
        opacity: 0.50
    }

    Label {
        anchors {
            bottom: bottomBar.top
            right: parent.right
            left: parent.left
            margins: units.gu(4)
        }

        // TODO only show on the fist time using this app
        visible: Applications.isLauncherEmpty

        text: i18n.tr('Swipe from the bottom edge to open the app list. Tap and hold apps to view details and place them on this screen.')
        color: systemSettings.dashBackground ? 'white' : 'black'

        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        verticalAlignment: Text.AlignVCenter
    }

    Item {
        id: bottomBar

        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: units.gu(1)
        }
        height: appHeight

        RowLayout {
            id: bottomBarLayout

            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            Repeater {
                model: Applications.favoriteList

                delegate: Launcher {
                    Layout.preferredHeight: appHeight
                    Layout.preferredWidth: appWidth

                    app: model
                    color: systemSettings.dashBackground ? 'white' : 'black'

                    onPressAndHold: showAppPage(model, index, false)
                }
            }
        }
    }

    Rectangle {
        id: overlay

        anchors.fill: parent
        visible: placingApp

        color: 'black'
        opacity: 0.5
    }

    Label {
        visible: placingApp && !dragMouseArea.pressed

        anchors {
            bottom: movingApp.top
            bottomMargin: units.gu(1)
            horizontalCenter: movingApp.horizontalCenter
        }

        text: i18n.tr('Drag app to place it on the grid')
        color: systemSettings.dashBackground ? 'white' : 'black'
    }

    Launcher {
        id: movingApp

        visible: placingApp

        height: appHeight
        width: appWidth

        app: appToPlace
        color: systemSettings.dashBackground ? 'white' : 'black'
        launchApp: false
    }

    MouseArea {
        id: dragMouseArea

        anchors.fill: parent

        propagateComposedEvents: true
        onPositionChanged: {
            movingApp.x = mouse.x - (movingApp.width / 2)
            movingApp.y = mouse.y - (movingApp.height / 2)
        }

        onReleased: {
            if (placingApp) {
                var column = parseInt(mouse.x / (appWidth + grid.columnSpacing));
                var row = parseInt(mouse.y / (appHeight + grid.rowSpacing));
                var index = column;
                if (row) {
                    index = (columns * row) + column;
                }

                if (index > (columns * rows)) {
                    Applications.insertFavoriteApp(column, appToPlace.id);
                }
                else {
                    Applications.insertLauncherApp(index, appToPlace.id);
                }

                placingApp = false;
                appToPlace = null;
            }
        }
    }

    AppsBottomEdge {
        id: bottomEdge

        height: parent.height
        width: parent.width

        onOpenSettings: pageStack.push(Qt.resolvedUrl('SettingsPage.qml'), {settings: settings})
        onShowApp: {
            var page = pageStack.push(Qt.resolvedUrl('AppPage.qml'), {app: app, addApp: true});
            page.onAddRemoveApp.connect(function(app) {
                appToPlace = app;

                movingApp.x = (parent.width / 2) - (movingApp.width / 2)
                movingApp.y = (parent.height / 2) - (movingApp.height / 2)

                placingApp = true;
            });
        }
    }
}
