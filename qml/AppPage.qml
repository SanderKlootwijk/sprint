import QtQuick 2.4
import QtQuick.Layouts 1.1
import Ubuntu.Components 1.3

import Sprint 1.0

Page {
    property var app
    property bool uninstalling: false
    property bool addApp: false

    signal addRemoveApp(var app)

    header: PageHeader {
        id: header
        title: app.name
    }

    ColumnLayout {
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            leftMargin: units.gu(1)
            rightMargin: units.gu(1)
        }

        spacing: units.gu(1)

        Image {
            Layout.preferredWidth: units.gu(30)
            Layout.preferredHeight: width
            Layout.alignment: Qt.AlignHCenter
            id: image

            source: {
                if (!app || !app.id) {
                    return '';
                }
                else if (IconPacks.hasCurrentIconPack) {
                    var icon = IconPacks.currentIconPack.getIcon(app.id);
                    if (icon) {
                        return icon;
                    }
                }

                return app.icon;
            }
            asynchronous: true

            fillMode: Image.PreserveAspectFit
            sourceSize {
                width: image.width
                height: image.height
            }
        }

        Label {
            text: app.comment
            visible: app.comment

            wrapMode: Text.WordWrap
        }

        RowLayout {
            Layout.fillWidth: true

            spacing: units.gu(1)

            // TODO confirmation popup
            Button {
                Layout.fillWidth: true

                text: i18n.tr('Uninstall')
                visible: !app.system && !uninstalling

                onClicked: {
                    uninstalling = true;
                    Applications.uninstall(app.packageName, app.version);
                }
            }

            ProgressBar {
                Layout.preferredWidth: launchButton.width
                Layout.fillWidth: true

                visible: uninstalling
                indeterminate: true
            }

            Button {
                id: launchButton

                Layout.fillWidth: true

                text: i18n.tr('Launch')
                color: UbuntuColors.green

                onClicked: {
                    Qt.openUrlExternally(app.uri);
                    pageStack.pop();
                }
            }

            Button {
                id: addRemoveButton

                Layout.fillWidth: true

                text: addApp ? i18n.tr('Add to grid') : i18n.tr('Remove from grid')

                onClicked: {
                    addRemoveApp(app);
                    pageStack.pop();
                }
            }
        }

        Item {
            id: spacer

            Layout.fillHeight: true
        }
    }

    Connections {
        target: Applications

        onUninstallFinished: {
            uninstalling = true;

            if (ok) {
                pageStack.pop();
            }
        }
    }
}
