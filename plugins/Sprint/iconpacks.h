#ifndef ICONPACKS_H
#define ICONPACKS_H

#include <QObject>
#include <QQmlListProperty>

#include "iconpack.h"

class IconPacks: public QObject {
    Q_OBJECT

    Q_PROPERTY(QQmlListProperty<IconPack> list READ list NOTIFY listChanged)
    Q_PROPERTY(bool hasCurrentIconPack READ hasCurrentIconPack NOTIFY currentIconPackChanged)
    Q_PROPERTY(IconPack* currentIconPack READ currentIconPack NOTIFY currentIconPackChanged)

public:
    IconPacks();
    ~IconPacks() = default;

    Q_INVOKABLE void refresh();
    Q_INVOKABLE IconPack* get(const QString &id);
    Q_INVOKABLE void setCurrentIconPack(const QString &id);

    QQmlListProperty<IconPack> list();
    IconPack* currentIconPack();
    bool hasCurrentIconPack();

Q_SIGNALS:
    void listChanged();
    void currentIconPackChanged();

private:
    Q_DISABLE_COPY(IconPacks)

    QList<IconPack *> m_iconPacks;
    IconPack *m_currentIconPack;
    bool m_hasCurrentIconPack = false;
};

#endif
