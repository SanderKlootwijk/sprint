#include <QDebug>
#include <QDirIterator>

#include "iconpacks.h"
#include "iconpack.h"

IconPacks::IconPacks() {
    refresh();
}

void IconPacks::refresh() {
    QDirIterator dir(QStringLiteral("/opt/click.ubuntu.com/"), QDir::Files | QDir::NoDotAndDotDot | QDir::Readable, QDirIterator::Subdirectories);

    while (dir.hasNext()) {
        dir.next();

        QString iconPackFile = dir.fileInfo().absoluteFilePath();
        if (iconPackFile.endsWith(QStringLiteral("icon-pack-data.json"))) {
            IconPack *pack = new IconPack(iconPackFile);

            m_iconPacks.append(pack);
        }
    }

    Q_EMIT listChanged();
}

QQmlListProperty<IconPack> IconPacks::list() {
    return QQmlListProperty<IconPack>(this, m_iconPacks);
}

IconPack* IconPacks::get(const QString &id) {
    IconPack *found = nullptr;

    if (!id.isEmpty()) {
        Q_FOREACH(IconPack *iconPack, m_iconPacks) {
            if (iconPack->id().startsWith(id)) {
                found = iconPack;
                break;
            }
        }
    }

    return found;
}

IconPack* IconPacks::currentIconPack() {
    return m_currentIconPack;
}

bool IconPacks::hasCurrentIconPack() {
    return m_hasCurrentIconPack;
}

void IconPacks::setCurrentIconPack(const QString &id) {
    m_currentIconPack = get(id);

    if (m_currentIconPack) {
        m_hasCurrentIconPack = true;
        m_currentIconPack->loadIcons();
    }
    else {
        m_hasCurrentIconPack = false;
    }

    Q_EMIT currentIconPackChanged();
}
